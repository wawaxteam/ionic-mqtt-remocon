# Simple MQTT remote control #

The demo application can be used with a MQTT broker to manage a dimmable light appliance remotely.

It has been tested on Android and iOS.


![MqttRC.PNG](https://bitbucket.org/repo/KoAA56/images/707127614-MqttRC.PNG)

## Framework ##

[Ionic2](http://ionicframework.com/)

## Getting started ##
- Download this project
- In Intel XDK -> Start New project -> __Import Project__ and point to the project folder
- Use Build tab to build apps for iOS, Android...

## Documentation for XDK ##
- [Intel XDK Overview](http://software.intel.com/en-us/html5/xdkdocs)