/*global angular,ionic,navigator,console*/

angular.module('RC', ['ionic','MQTT'])

.controller('RCCtrl',['$scope', '$rootScope', 'MqttService', function($scope, $rootScope, MqttService) {

    ionic.Platform.ready(function() {
        navigator.splashscreen.hide();
    });

    var MQTT_MESSAGE = "MSG";
    var MQTT_INFO = "INF";
    var MQTT_ERROR = "ERR";

    $scope.isConnected = false;
    $scope.hideList = false;

    /* Connection settings */
    $scope.brokerSettings = {
        url: "test.mosquitto.org",
        //url: "broker.mqttdashboard.com",
        port: 8080
    };

    /* List of Mqtt bindings */
    $scope.bindings = [
        { label: 'command', channel: 'ldali/lights/cmd', type: 'output' },
        { label: 'status', channel: 'ldali/lights/status', type: 'input' }
    ];

    /* Message JSON format { timestamp: DATE), eventType: DEFINED_TYPE, eventMessage: MESSAGE_STRING }*/
    $scope.mqttEvents = [];

    /* Connection information */
    $scope.connectionInformation = {};

    /* Light information */
    //$scope.level = 0;
    $scope.dimmingValue = 0;

    MqttService.onMessage(function(message) {
        CreateLog(MQTT_MESSAGE,'Incoming message: ' + message.payloadString + ', on channel: '+message.destinationName);

        //When we receive "status message"
        if(message.destinationName === $scope.bindings[1].channel){
            //$scope.status = message.payloadString;
            $scope.dimmingValue = message.payloadString;
            $scope.$apply();
        }
        //When we receive other type of message
        /*else if(message.destinationName === $scope.bindings[2].channel){
            $scope.level = Number(message.payloadString);
            console.log("Light level has changed to: "+message.payloadString);
        }*/
        else{
            //Nothing to do
        }
        $scope.$apply();//we are out of the scope
    });

    $rootScope.$on('isConnected',function(event) {
        CreateLog(MQTT_INFO,"Connection is established");

        $scope.isConnected = true;
        $scope.$apply();//we are out of the scope

        //subscribe to channel
        MqttService.subscribe($scope.bindings[1].channel);//dimming level feedback % [0-100]}
        CreateLog(MQTT_INFO,'Subscription to: '+$scope.bindings[1].channel);

        //connection status
        $scope.connectionInformation = MqttService.connectionInformation();
    });

    $scope.toggleLightON = function() {
        if($scope.isConnected){
            MqttService.publish($scope.bindings[0].channel,"100");
            CreateLog(MQTT_MESSAGE,"Light should switch on");
        }
    };

    $scope.toggleLightOFF = function() {
        if($scope.isConnected){
            MqttService.publish($scope.bindings[0].channel,"0");
            CreateLog(MQTT_MESSAGE,"Light should switch off");
        }
    };

    $scope.connect = function(){
        try{
            MqttService.connect($scope.brokerSettings.url, $scope.brokerSettings.port);
        }
        catch(exception){
            CreateLog(MQTT_ERROR,exception);
        }
    };

    $scope.disconnect = function(){
        MqttService.disconnect();
        $scope.isConnected = false;
        CreateLog(MQTT_INFO,"Disconnected from Mqtt broker");
    };

    /* On range update */
    $scope.updateDimming = function(value){
        MqttService.publish($scope.bindings[0].channel,value);
        $scope.dimmingValue = value;
        CreateLog(MQTT_MESSAGE,'Dimming to: '+value+'%');
    };

    /* UI behaviour */
    $scope.toggleShowHideList = function(){
        $scope.hideList = !$scope.hideList;
    };

    $scope.ContentController = function($scope, $ionicSideMenuDelegate) {
        $scope.toggleLeft = function() {
            $ionicSideMenuDelegate.toggleLeft();
        };
    };

    /***********************************************************************/
    /* UTILS
    /***********************************************************************/

    /* Message forge eventType{MQTT_INFO, MQTT_ERROR, MQTT_MESSAGE} */
    function CreateLog(eventType, eventMessage){
        $scope.mqttEvents.unshift({
            timestamp: new Date().toLocaleString(),
            eventType: eventType,
            eventMessage: eventMessage
        });
        console.log("At: "+new Date()+", Event: "+eventType+", Message:"+eventMessage);
    }

}]);
