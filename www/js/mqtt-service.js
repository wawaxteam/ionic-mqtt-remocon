(function () {
    "use strict";

    /*global console,alert,mqtt,angular,document,$,Paho*/

    angular.module('MQTT', [])

    .factory('MqttService', ['$rootScope', function ($rootScope) {

        var service = {};

        var client = {};
        var topics = [];//TODO to remove if not used

        return {

            /* Connect to the broker */
            connect: function (host, port, user, password) {

                // Create a client instance
                client = new Paho.MQTT.Client(host, port, "html5RC" + Math.random(100000));

                // Set callback handlers
                client.onConnectionLost = onConnectionLost;
                client.onMessageArrived = onMessageArrived;

                // Connexion options
                var options = {
                    //Connection attempt timeout in seconds
                    timeout: 5,
                    //Gets Called if the connection has successfully been established
                    onSuccess: onConnect,
                    //Gets Called if the connection could not be established
                    onFailure: function (message) {
                        alert("Connection failed: " + message.errorMessage);
                        console.log(message);
                    }
                };

                // Connect the client
                console.log("Try to connect to MQTT Broker " + host);
                client.connect(options);

                // Called when the client loses its connection
                function onConnectionLost(responseObject) {
                    if (responseObject.errorCode !== 0) {
                        console.log("onConnectionLost: " + responseObject.errorMessage);
                        $rootScope.$emit('evtConnectionLost');
                    }
                }

                // Called when mqtt message arrived
                function onMessageArrived(message) {
                    service.callback(message);
                }

                // Called when the client connects
                function onConnect(){
                    console.log("onConnect callback");
                    $rootScope.$emit('isConnected');
                }

            },

            /* Send message */
            publish: function (topic, payload) {
                var message = new Paho.MQTT.Message(payload);
                message.destinationName = topic;
                client.send(message);
                console.log('publish message: ' + message.payloadString + ' with topic: ' + message.destinationName);
            },

            /* Receive message from subscribed topics */
            onMessage: function (message) {
                console.log('received message: ' + message.payloadString);
                service.callback = message;
            },

            /* Subscribe to a new topic */
            subscribe: function (topic) {
                console.log('subscribe to: ' + topic);
                client.subscribe(topic);
                topics.push(topic);
            },

            /* Disconnect to the broker */
            disconnect: function () {
                client.disconnect();
                console.log("Client disconnected");
            },

            /* Information about the connection */
            connectionInformation: function (){
                var connectionInfo =  {
                    clientID: client.clientId,
                    brokerURL: client.host,
                    brokerPort: client.port
                };
                return connectionInfo;
            }
        };
    }]);

})();
